#!/bin/bash

if [ ! "$(whoami)" = "root" ]; then
    echo "Superuser Privelleges are needed to import the newer public.key file for the Feren OS Repositories to your Feren OS Machine. Please enter your password to continue:"
    sudo "$0"
    exit $?
fi

wget https://gitlab.com/feren-os/feren-repositories-bionic/raw/master/repositories/public.key -O /usr/share/keyrings/feren-os.key
/usr/bin/apt-key --keyring /etc/apt/trusted.gpg.d/feren-os.gpg add /usr/share/keyrings/feren-os.key

apt update
echo "Alright, all done, the repository should resume functionality on this Feren OS installation. Exiting."
exit 0
